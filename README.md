# Relief Site

## Context
This site is meant to be a repository of locations offering aid. Those that are seeking aid, or would like to 
contribute in the relief effort can use the site to fine locations near them 

## Quick Start
The data is pulled from a google excel sheet and outputted into a list as well as markers on the open layers map. Use the search input field to filter the list as required. You can search by name, address or cardinal point. On first load the website asks for your location as a default location. Click on the markers to view information about the location. Click on the item information links to trigger the appropriate actions

## Modifications
Edit the date from the google sheet to update the list. The list markup is a bunch of **text/template** located in the html markup itself. Edit the templates as necessary to change the elements of the list item info.