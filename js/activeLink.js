var url = window.location.href;
var activeLink = url.substring(url.lastIndexOf('/') + 1);
$('a[href="' + activeLink + '"]').addClass('active');

function makeLinkActive() {
  $('a[href="index.html#dropoff"]').addClass('active');
  $('button.navbar-toggler').attr('aria-expanded', false);
  $('div.navbar-collapse').removeClass('show');
};